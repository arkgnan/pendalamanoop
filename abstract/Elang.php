<?php

require_once 'Fight.php';

class Elang extends Fight{

    public function __construct($jumlahKaki = 2, $keahlian = 'terbang tinggi', $attackPower = 10, $defencePower = 5){
        // jumlahKaki bernilai 2, dan keahlian bernilai “terbang tinggi”, attackPower = 10 , deffencePower = 5 
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;

    }

    public function getInfoHewan($target=array(),$nama=null) {
        // set defaul name jika tidak dikasih nama
        if($nama == null) {
            $this->nama = 'Elang';
        } else {
            $this->nama = $nama;
        }
        $string = "jenis_hewan : Harimau<br/>
        nama : {$this->nama}<br/>
        darah : {$this->darah}<br/>
        jumlahKaki : {$this->jumlahKaki}<br/>
        keahlian : {$this->keahlian}<br/>
        attackPower : {$this->attackPower}<br/>
        defencePower : {$this->defencePower}<hr/>
        atraksi : {$this->atraksi()}<hr/>
        <strong>Fight</strong><br/>
        serang : {$this->serang($target['nama'])}<br/>
        diserang : {$this->diserang($target)}";
        return  $string;
    }
}