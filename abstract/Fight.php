<?php

require_once 'Hewan.php';
// abstract agar tidak bisa di intansiasi
// extend ke hewan agar bisa menggunakan property & method dari class hewan
abstract class Fight extends Hewan {
    // set ke public agar tidak perlu menggunakan setter & getter
    public  $attackPower,
            $defencePower;

    protected function serang($target){
        return $target.' sedang menyerang '.$this->nama;
    }

    protected function diserang($target){
        // efek diserang : darah sekarang – attackPower penyerang / defencePower yang diserang
        $darah_sekarang = $this->darah;
        $this->darah = $darah_sekarang - $target['attack'] / $this->defencePower;
        return "{$this->nama} sedang diserang!<br/>sisa darah {$this->nama} : {$this->darah} ({$darah_sekarang} - {$target['attack']} / {$this->defencePower})";
    }
}