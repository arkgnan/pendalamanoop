<?php

require_once 'Fight.php';

class Harimau extends Fight {

    public function __construct($jumlahKaki = 4, $keahlian = 'lari cepat', $attackPower = 7, $defencePower = 8){
        // jumlahKaki bernilai 4, dan keahlian bernilai “lari cepat” , attackPower = 7 , deffencePower = 8
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;

    }

    public function getInfoHewan($target=array(),$nama=null) {
        // set defaul name jika tidak dikasih nama
        if($nama == null) {
            $this->nama = 'Harimau';
        } else {
            $this->nama = $nama;
        }
        $string = "jenis_hewan : Harimau<br/>
        nama : {$this->nama}<br/>
        darah : {$this->darah}<br/>
        jumlahKaki : {$this->jumlahKaki}<br/>
        keahlian : {$this->keahlian}<br/>
        attackPower : {$this->attackPower}<br/>
        defencePower : {$this->defencePower}<hr/>
        atraksi : {$this->atraksi()}<hr/>
        <strong>Fight</strong><br/>
        serang : {$this->serang($target['nama'])}<br/>
        diserang : {$this->diserang($target)}";
        return  $string;
    }

}