<?php

// abstract agar tidak bisa di intansiasi
abstract class Hewan  {
    // set ke public agar tidak perlu menggunakan setter & getter
    public  $nama,
            $darah = 50,
            $jumlahKaki,
            $keahlian;

    protected function atraksi() {
        return $this->nama. ' sedang ' .$this->keahlian; 
    }
}