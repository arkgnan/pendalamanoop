<?php

// menggunakan trait
trait Hewan  {
    // set ke public agar tidak perlu menggunakan setter & getter
    public  $nama,
            $darah = 50,
            $jumlahKaki,
            $keahlian;

    protected function atraksi() {
        return $this->nama. ' sedang ' .$this->keahlian; 
    }
}