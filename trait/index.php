<?php

require_once 'Elang.php';
require_once 'Harimau.php';


$e = new Elang;
$e->nama = 'Manuk';
$elang = $e->nama;
$elang_attack = $e->attackPower;
$elang_defence = $e->defencePower;

$h = new Harimau;
$h->nama = 'Simba';
$harimau = $h->nama;
$harimau_attack = $h->attackPower;
$harimau_defence = $h->defencePower;

$harimau_data = array(
    'nama'=>$harimau,
    'attack'=>$harimau_attack,
    'defence'=>$harimau_defence
);

$elang_data = array(
    'nama'=>$elang,
    'attack'=>$elang_attack,
    'defence'=>$elang_defence
);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 - Pendalaman OOP</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid">
        <div class="row py-3">
            <?php
            $end_match = false;
            $winner = "";
            $round = 1;
            while($end_match==false){
            ?>
            <div class="col-12 py-2 order-2">
                <div class="card-group">
                    <div class="card bg-light">
                        <div class="card-body">
                        <?php
                            $elang_serang = $elang_data;
                            echo $h->getInfoHewan($elang_serang, $h->nama);
                            if ($h->darah <= 0) {
                                $end_match = true;
                                $winner = $e->nama;
                            }
                        ?>
                        </div>
                    </div>

                    <div class="card bg-light">
                        <div class="card-body">
                        <?php
                            $harimau_serang = $harimau_data;
                            echo $e->getInfoHewan($harimau_serang, $e->nama);
                            if ($e->darah <= 0) {
                                $end_match = true;
                                $winner = $h->nama;
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $round++;
            }
            ?>
            <div class="col-12 order-1">
                <h5 class="my-0">Pemenangnya adalah <?=$winner;?></h5>
                <p>pertandingan berakhir setelah <?=$round;?> ronde</p>
            </div>
        </div>
    </div>
</body>
</html>
